import {Component} from '@angular/core';
import {OnInit} from '@angular/core';

import {User} from '../classes/user';
import {HttpService} from '../services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user: User;

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.getData().subscribe((data: User) => this.user = data);
  }
}
